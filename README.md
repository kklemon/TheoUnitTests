# Theo Unit Tests

**DENKT DRAN ASSERTIONS EINZUSCHALTEN WENN IHR DIE UNIT TESTS DURCHFÜHRT**

Damit nicht jede zweite Woche ständig nach Tests gefragt werden muss und die gleichen Sachen wieder und wieder in
alle möglichen Gruppen geschrieben werden.

Commits werden über Merge Requests geregelt, damit keiner Unfug betreibt und auch geprüft werden kann, ob die Tests
auch durchlaufen (ein Denkfehler kann ja gerne mal vorkommen, vor Allem, wenn man die Tests schreibt, ohne das Programm 
fertig zu haben). Wenn man sich unsicher ist, wie Merge Requests funktionieren, kann man sich hier erkundigen:
https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html. Es ist dafür durchaus sinnvoll ein Mirror Repository
einzurichten: https://about.gitlab.com/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/

Die ``.gitignore``-Datei ist so aufgebaut, dass man das ganze Repo eigentlich als Projekt für seine Theo-Hausaufgaben
benutzen kann und trotzdem nur die Unit Tests gepusht werden. Es kann gerne alles mögliche, das
diese Einschränkung nicht verletzt (alle möglichen IDE-Dateien usw.), hinzugefügt werden.

Wenn du meistens sehr früh deine Aufgaben schon bearbeitet hast, wäre es super, wenn du dich bei mir melden könntest,
da ich noch Leute brauch, die die Merge Requests reviewen (ein Mal bei sich ausführen, wenn die eigene Abgabe schon als
richtig bewertet wurde, um zu gucken, dass der Unit Test auch auf die richtigen Werte prüft).

Für die, die noch ein paar Tipps brauchen: https://docs.google.com/document/d/1me3dK3EHwz_PLjuqX0U1oV0P3YCuCVVKlTvBIvKF7A8/edit#