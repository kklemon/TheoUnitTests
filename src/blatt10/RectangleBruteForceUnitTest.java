import static org.junit.jupiter.api.Assertions.assertEquals;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;
import java.util.Random;

/*
Diese Tests sind Bruteforce Tests und beachten bei weitem nicht alle cases.
Mit randomPermutations() steht zwar ein Test zur Verfügung, der zufällige Kombinationen erzeugt und prüft,
dieser kann unter Umständen jedoch nicht alle fehlererzeugenden Kombinationen erfassen. Ein Mehrfachausführen
bzw. erhöhen der Kombinationszahlen, sollte jedoch mit hoher Wahrscheinlich vorhandene Fehler aufdecken.
 */

public class RectangleTest {

    @Test
    public void empty() {
        TuringMachine tm = Rectangle.rectangleMachine();
        assertEquals(Simulate.simulate(tm, "", 1000000), Simulate.Result.STUCK);
    }

    @Test
    public void initial() {
        TuringMachine tm = Rectangle.rectangleMachine();
        assertEquals(Simulate.simulate(tm, "^<v", 1000000), Simulate.Result.STUCK);
        assertEquals(Simulate.simulate(tm, "><v", 1000000), Simulate.Result.STUCK);
        assertEquals(Simulate.simulate(tm, ">^v", 1000000), Simulate.Result.STUCK);
        assertEquals(Simulate.simulate(tm, ">^<", 1000000), Simulate.Result.STUCK);
        assertEquals(Simulate.simulate(tm, "><", 1000000), Simulate.Result.STUCK);
        assertEquals(Simulate.simulate(tm, "^v", 1000000), Simulate.Result.STUCK);
    }

    @Test
    public void allImpossibleInOrder() {
        TuringMachine tm = Rectangle.rectangleMachine();

        for (int k = 0; k < 200; k++) {
            for (int x = 0; x <= k; x++) {
                for (int y = 0; y <= k; y++) {
                    for (int z = 0; z <= k; z++) {
                        for (int w = 0; w <= k; w++) {

                            if (x + y + z + w != k) continue;
                            if (x == z && y == w) continue;

                            System.out.println("Length:" + k + ", " + x + ":" + y + ":" + z + ":" + w);

                            StringBuilder sb = new StringBuilder();

                            for (int i = 0; i < x; i++) {
                                sb.append('>');
                            }

                            for (int i = 0; i < y; i++) {
                                sb.append('^');
                            }

                            for (int i = 0; i < z; i++) {
                                sb.append('<');
                            }

                            for (int i = 0; i < w; i++) {
                                sb.append('v');
                            }

                            assertEquals(Simulate.simulate(tm, sb.toString(), 1000000), Simulate.Result.STUCK);

                        }
                    }
                }
            }
        }
    }


    @Test
    public void allPossible() {
        TuringMachine tm = Rectangle.rectangleMachine();

        for (int x = 1; x < 100; x++) {
            for (int y = 1; y < 100; y++) {
                if (x + y > 100) continue;

                System.out.println(x + ":" + y);

                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < x; i++) {
                    sb.append('>');
                }

                for (int i = 0; i < y; i++) {
                    sb.append('^');
                }

                for (int i = 0; i < x; i++) {
                    sb.append('<');
                }

                for (int i = 0; i < y; i++) {
                    sb.append('v');
                }

                assertEquals(Simulate.simulate(tm, sb.toString(), 1000000), Simulate.Result.ACCEPTED);
            }
        }
    }

    @Test
    public void randomPermutations() {
        TuringMachine tm = Rectangle.rectangleMachine();

        Random rand = new Random();

        for (int k = 1; k < 200; k++) {
            for (int x = 0; x < (k < 10 ? 10000 : 1000); x++) {
                StringBuilder sb = new StringBuilder();

                int i = 0;
                int j = 0;
                int l = 0;
                int m = 0;

                boolean accepted = true;

                for (int y = 0; y < k; y++) {
                    int choice = rand.nextInt(4);

                    switch (choice) {
                        case 0:
                            sb.append(">");
                            accepted = accepted && j + l + m == 0;
                            i++;
                            break;
                        case 1:
                            sb.append("^");
                            accepted = accepted && i > 0 && j + m == 0;
                            l++;
                            break;
                        case 2:
                            sb.append("<");
                            accepted = accepted && i > 0 && l > 0 && m == 0;
                            j++;
                            break;
                        case 3:
                            sb.append("v");
                            accepted = accepted && i > 0 && j > 0 && l > 0;
                            m++;
                            break;
                    }
                }

                Simulate.Result result = Simulate.simulate(tm, sb.toString(), 1000000);

                if (accepted && i == j && i > 0 && l == m && l > 0) {
                    assertEquals(Simulate.Result.ACCEPTED, result);
                }
                else {
                    assertEquals(Simulate.Result.STUCK, result);
                }
            }
        }
    }
}
